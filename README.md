# Kulay #

Kulay is a programming language I'm creating in order to prepare myself to
learn about compilers & VMs.  It's clearly a toy project for now, as, at this
day (01/21/15) it only support additions and substractions, but the pipeline is
complete.

### What's in here? ###

* `src/comp/` is a compiler to pseudo-asm. It only handles additions and
    substractions of constants, like `1 - 2 + 3;`. Will obviously be extended
* `src/asm/` assembles the pseudo-asm to Kulay bytecode
* `src/aot/` will be a ahead-of-time compiler. Now, it's just crappy code.
* `src/interp/` is just a bytecode interpreter.
* `src/jit/` will be a jitter. Now, it's hard to really affirm it's a jitter.
* `src/common/` is the common and platform independent code shared by those apps.
* `src/x86/` contains the x86 code dependent stuff.

### How do I get set up? ###

You need a Linux x86 system, make, and a C compiler. Either clang or gcc works.

* `make`, and you're done
* `make test` compiles and runs the test(s)

If you want to be able to run the tests, you'll also need:

* `diff`
* `objdump`

### Contribution guidelines ###

Don't. You'll lose your time, and make me learn less.

### Who do I talk to? ###

* Vermeille.

http://Vermeille.fr
