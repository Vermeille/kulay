#define _DEFAULT_SOURCE
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdint.h>

#include <common/opcodes.h>
#include <common/tiles.h>
#include <common/allocator.h>
#include <common/vm.h>

#include <x86/emitter.h>

void reg_insert(struct ir_node *n);
void allocate(struct stack_allocation_mgr *s);
void dot_print(struct ir_node *n);

int main(int argc, char** argv)
{
    if (argc < 2)
        return 1;

    struct stat sb;
    const char *prgm;

    int fd = open(argv[1], O_RDONLY);
    fstat(fd, &sb);

    prgm = (const char*)mmap(NULL, sb.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0);

    struct stack_allocation_mgr alloc;
    init_stack_allocator(&alloc);

    VM vm(prgm);
#if 0
    tree = make_expr_tree(prgm, &rdptr);
    reg_insert(tree);

    int a;
    liveness_analysis(&alloc, tree);
    allocate(&alloc);
    dot_print(tree);
#endif

    vm.Compile();
    return 0;
}
