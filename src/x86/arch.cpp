/*
 * x86arch.c
 * Copyright (C) 2015 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

#include <cassert>

#include "arch.h"

std::string X86::RegName(Regs r)
{
    switch (r)
    {
        case X86::eax:
            return "%eax";
        case X86::ecx:
            return "%ecx";
        case X86::edx:
            return "%edx";
        case X86::ebx:
            return "%ebx";
        case X86::esp:
            return "%esp";
        case X86::ebp:
            return "%ebp";
        case X86::esi:
            return "%esi";
        case X86::edi:
            return "%edi";
        case X86::noreg:
            return "NOREG";
    }
    // can't be reached
    assert(false);
    return "ERROR";
}

void X86::Gpregs(Regs *r)
{
    r[5] = eax;
    r[4] = ecx;
    r[3] = edx;
    r[2] = ebx;
    r[1] = esi;
    r[0] = edi;
}

