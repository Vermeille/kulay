#pragma once

#include <common/opcodes.h>
#include <x86/arch.h>
#include <map>
#include <vector>

struct AddrReg
{
    X86::Regs r;
};

struct Addr
{
    Addr(int a) : addr(a) {}
    int addr;
};

struct Operand
{
    Operand(X86::Regs r);
    Operand(X86::Regs r, int disp);
    Operand(AddrReg addr);
    Operand(Addr addr);
    Operand(int d, X86::Regs base, X86::Regs idx, unsigned char scale);

    regs base;
    regs index;
    int disp;
    byte scale;
    byte is_mem;
    byte is_imm;
};

/*
** Emit code, GAS convention: <instr> <src> <dst>
*/
class X86Emitter
{
    public:
        X86Emitter();
        X86Emitter(byte* buffer) : xpage_(buffer), xpage_cur_(buffer) {}

        typedef int (*compiled)(void);

        compiled Compile();
        uint32_t SizeSoFar() const { return xpage_ - xpage_cur_; }
        int GetLocalVarOffset(int idx) { return -(idx + 1) * sizeof (int); }

        void And(Operand o, regs r);

        void add(Operand o, regs r);
        void add(regs r, Operand o);
        void add(int imm, Operand o);

        void sub(Operand o, regs r);
        void sub(regs r, Operand o);
        void sub(int imm, Operand o);

        void mul(Operand o, regs r);
        void mul(int imm, regs r);
        void mul(int imm, Operand o, regs r);

        void cmp(Operand o, X86::Regs r);
        void cmp(X86::Regs r, Operand o);

        void setl(X86::Regs r);
        void setle(X86::Regs r);
        void setg(X86::Regs r);
        void setge(X86::Regs r);
        void sete(X86::Regs r);
        void setne(X86::Regs r);

        void push(int imm);
        void push(Operand r);
        void push(regs r) { push(Operand(r)); }

        void mov(regs r, Operand o);
        void mov(Operand o, regs r);
        void mov(int imm, Operand o);

        void movzb(Operand o, X86::Regs r);

        void pop(regs r);

        void jne(unsigned int bc_addr);
        void jmp(unsigned int bc_addr);

        void ret();

        void WriteBCAddr(unsigned int bcaddr);
        void DeclareBCAddr(unsigned int bcaddr);

    private:
        void SIB(Operand o, int r2);
        void SIB(Operand o, X86::Regs r2);

        void ModRM(Operand o, int r2);
        void ModRM(Operand o, X86::Regs r2);

        byte* xpage_;
        byte* xpage_cur_;

        std::map<unsigned int, std::vector<byte*>> unresolved_;
        std::map<unsigned int, byte*> bc_mapping_;
};
