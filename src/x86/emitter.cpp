#include "emitter.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/mman.h>
#include <unistd.h>

#include <iostream>

Operand::Operand(X86::Regs r) :
        base(r),
        index(X86::noreg),
        disp(0),
        scale(1),
        is_mem(0),
        is_imm(0)
{
}

Operand::Operand(AddrReg r) :
        base(r.r),
        index(X86::noreg),
        disp(0),
        scale(1),
        is_mem(1),
        is_imm(0)
{
}

Operand::Operand(Addr i) :
        base(X86::noreg),
        index(X86::noreg),
        disp(i.addr),
        scale(0),
        is_mem(1),
        is_imm(0)
{
}

Operand::Operand(X86::Regs r, int i) :
        base(r),
        index(X86::noreg),
        disp(i),
        scale(0),
        is_mem(1),
        is_imm(0)
{
}

Operand::Operand(int d, X86::Regs base, X86::Regs idx, unsigned char scale) :
        base(base),
        index(idx),
        disp(d),
        scale(scale),
        is_mem(1),
        is_imm(0)
{
}

void X86Emitter::SIB(Operand o, int r2)
{
    (void) r2;

    if (o.base == X86::ebp)
    {
        fprintf(stderr, "ebp SIB not implemented\n");
        exit(1);
    }

    byte mod;

    switch (o.scale)
    {
        case 0:
        case 1:
            mod = 0;
            break;
        case 2:
            mod = 1;
            break;
        case 4:
            mod = 2;
            break;
        case 8:
            mod = 3;
            break;
        default:
            fprintf(stderr, "scale is plain wrong dude\n");
            exit(1);
    }

    if (o.index == X86::noreg)
    {
        *xpage_ = (mod << 6) | (4 << 3) | o.base;
        ++xpage_;
    }
    else
    {
        *xpage_ = (mod << 6) |(o.index << 3) | o.base;
        ++xpage_;
    }
}

void X86Emitter::SIB(Operand o, X86::Regs r2)
{
    SIB(o, static_cast<int>(r2));
}

void X86Emitter::ModRM(Operand o, int r2)
{
    // [reg]
    if (o.is_mem && (o.disp == 0))
    {
        if ((o.scale >= 1 && o.disp == 0) || (o.base == X86::esp)) // [sib]
        {
            *xpage_ = 0x00 | (r2 << 3) | 4;
            ++xpage_;
            SIB(o, r2);
        }
        else if (o.is_imm) // disp32
        {
            *xpage_ = 0x00 | (r2 << 3) | 5;
            ++xpage_;
            *(int*)xpage_ = o.disp;
            xpage_ += 4;
        }
        else // [reg]
        {
            if (o.base == X86::ebp)
            {
                *xpage_ = (1 << 6) | (r2 << 3) | o.base;
                ++xpage_;
                *xpage_ = (char)o.disp;
                ++xpage_;
            }
            else
            {
                *xpage_ = 0x00 | (r2 << 3) | o.base;
                ++xpage_;
            }
        }
    }
    else if (o.is_mem && CHAR_MIN <= o.disp && o.disp <= CHAR_MAX)
    {
        if ((o.scale >= 1) || (o.base == X86::esp)) // [sib]+disp8
        {
            *xpage_ = (1 << 6) | (r2 << 3) | 4;
            ++xpage_;
            SIB(o, o.base);
            *xpage_ = (char)o.disp;
            ++xpage_;
        }
        else // [reg]+disp8
        {
            *xpage_ = (1 << 6) | (r2 << 3) | o.base;
            ++xpage_;
            *xpage_ = (char)o.disp;
            ++xpage_;
        }
    }
    else if (o.is_mem)
    {
        if ((o.scale >= 1) || (o.base == X86::esp)) // [sib]+disp32
        {
            *xpage_ = (2 << 6) | (r2 << 3) | 4;
            ++xpage_;
            SIB(o, r2);
            *(int*)xpage_ = o.disp;
            xpage_ += 4;
        }
        else // [reg]+disp32
        {
            *xpage_ = (2 << 6) | (r2 << 3) | o.base;
            ++xpage_;
            *(int*)xpage_ = o.disp;
            xpage_ += 4;
        }
    }
    else
    {
        *xpage_ = (3 << 6) | (r2 << 3) | o.base;
        ++xpage_;
    }
}

void X86Emitter::ModRM(Operand o, X86::Regs r2)
{
    ModRM(o, static_cast<int>(r2));
}

X86Emitter::X86Emitter() :
    xpage_((byte*)mmap(NULL, 1024*1024,
            PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS,
            -1, 0)),
    xpage_cur_(xpage_)
{}

void X86Emitter::And(Operand o, X86::Regs r)
{
    *xpage_ = 0x23;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::add(int imm, Operand o)
{
    *xpage_ = 0x81;
    ++xpage_;
    ModRM(o, 0);
    *(int*)xpage_ = imm;
    xpage_ += 4;
}

void X86Emitter::add(Operand o, X86::Regs r)
{
    *xpage_ = 0x03;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::add(X86::Regs r, Operand o)
{
    *xpage_ = 0x01;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::push(int imm)
{
    if (CHAR_MIN <= imm && imm <= CHAR_MAX)
    {
        *xpage_ = 0x6A;
        ++xpage_;
        *xpage_ = (char)imm;
        ++xpage_;
    }
    else
    {
        *xpage_ = 0x68;
        ++xpage_;
        *(int*)xpage_ = imm;
        xpage_ += 4;
    }
}

void X86Emitter::push(Operand r)
{
    if (!r.is_mem)
    {
        *xpage_ = 0x50 + r.base;
        ++xpage_;
    }
    else
    {
        *xpage_ = 0xFF;
        ++xpage_;
        ModRM(r, 6);
    }
}

void X86Emitter::pop(X86::Regs r)
{
    *xpage_ = 0x58 + r;
    ++xpage_;
}

void X86Emitter::ret()
{
    *xpage_ = 0xC3;
    ++xpage_;
}

void X86Emitter::mov(X86::Regs r, Operand o)
{
    *xpage_ = 0x89;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::mov(Operand o, X86::Regs r)
{
    *xpage_ = 0x8b;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::mov(int imm, Operand o)
{
    if (!o.is_mem)
    {
        assert(o.base <= X86::edi);
        *xpage_ = 0xB8 + o.base;
        ++xpage_;
        *(int*)xpage_ = imm;
        xpage_ += 4;
    }
    else
    {
        *xpage_ = 0xC7;
        ++xpage_;
        ModRM(o, 0);
        *(int*)xpage_ = imm;
        xpage_ += 4;
    }
}


void X86Emitter::sub(int imm, Operand o)
{
    *xpage_ = 0x81;
    ++xpage_;
    ModRM(o, 5);
    *(int*)xpage_ = imm;
    xpage_ += 4;
}

void X86Emitter::sub(Operand o, X86::Regs r)
{
    *xpage_ = 0x2B;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::sub(X86::Regs r, Operand o)
{
    *xpage_ = 0x29;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::mul(Operand o, X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0xAF;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::mul(int imm, X86::Regs dst)
{
    *xpage_ = 0x69;
    ++xpage_;
    ModRM(Operand(dst), dst);
    *(int*)xpage_ = imm;
    xpage_ += 4;
}

void X86Emitter::mul(int imm, Operand o, X86::Regs r)
{
    *xpage_ = 0x69;
    ++xpage_;
    ModRM(o, r);
    *(int*)xpage_ = imm;
    xpage_ += 4;
}

void X86Emitter::setl(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x9C;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::setle(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x9E;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::setg(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x9F;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::setge(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x9D;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::sete(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x94;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::setne(X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x95;
    ++xpage_;
    ModRM(Operand(r), 0);
}

void X86Emitter::cmp(Operand o, X86::Regs r)
{
    *xpage_ = 0x3B;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::cmp(X86::Regs r, Operand o)
{
    *xpage_ = 0x39;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::movzb(Operand o, X86::Regs r)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0xB6;
    ++xpage_;
    ModRM(o, r);
}

void X86Emitter::jne(unsigned int bcaddr)
{
    *xpage_ = 0x0F;
    ++xpage_;
    *xpage_ = 0x85;
    ++xpage_;
    WriteBCAddr(bcaddr);
}

void X86Emitter::jmp(unsigned int bcaddr)
{
    *xpage_ = 0xE9;
    ++xpage_;
    WriteBCAddr(bcaddr);
}

X86Emitter::compiled X86Emitter::Compile()
{
    return reinterpret_cast<compiled>(xpage_cur_);
}

void X86Emitter::WriteBCAddr(unsigned int bcaddr)
{
    auto addr = bc_mapping_.find(bcaddr);
    if (addr == bc_mapping_.end())
        unresolved_[bcaddr].push_back(xpage_);
    else
        *(int*)xpage_ = addr->second - xpage_ - 4;
    xpage_ += 4;
}

void X86Emitter::DeclareBCAddr(unsigned int bcaddr)
{
    bc_mapping_[bcaddr] = xpage_;
    for (auto& addr : unresolved_[bcaddr])
        *(int*)addr = xpage_ - addr - 4;
    unresolved_.erase(bcaddr);
}

