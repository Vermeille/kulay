#pragma once
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include <string>

class X86
{
    public:
        enum Regs
        {
            noreg = -1,
            eax = 0,
            ecx,
            edx,
            ebx,
            esp,
            ebp,
            esi,
            edi,
        };
        static void Gpregs(Regs *r);
        static std::string RegName(Regs r);
};

typedef X86::Regs regs;

#define NBREGS 6

