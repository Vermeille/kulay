#include "allocator.h"

#include <common/opcodes.h>
#include <common/tiles.h>

#include <stdlib.h>
#include <stdio.h>

struct liveness *push_tmp(struct stack_allocation_mgr *stack, int here)
{
    struct liveness *cur = &stack->lives[stack->nbr_lives];
    ++stack->nbr_lives;
    stack->stack[stack->stack_top] = cur;
    ++stack->stack_top;
    cur->definition = here;
    cur->last_use = here;
    cur->alloc.where = allocated::UNALLOC;
    cur->alloc.reg = X86::noreg;
    return cur;
}

struct liveness *use_top_tmp(struct stack_allocation_mgr *stack, int here)
{
    return use_tmp(stack, -1, here);
}

struct liveness *use_tmp(struct stack_allocation_mgr *stack,int offset, int here)
{
    struct liveness *cur = stack->stack[stack->stack_top + offset];
    cur->last_use = here;
    return cur;
}

struct liveness *pop_tmp(struct stack_allocation_mgr *stack, int here)
{
    struct liveness *cur = use_tmp(stack, -1, here);
    --stack->stack_top;
    return cur;
}

void init_stack_allocator(struct stack_allocation_mgr *s)
{
    s->stack_top = 0;
    s->nbr_lives = 0;
    s->lives = (liveness*)malloc(sizeof (struct liveness) * MAX_LIVENESS);
    s->remaining_regs = NBREGS;
    X86::Gpregs(s->free_regs);
    s->mem_bitset = ~0;
}

void print_location(const struct liveness *a)
{
    fprint_location(stdout, a);
}

void fprint_location(FILE *out, const struct liveness *a)
{
    if (!a)
        return;

    if (a->alloc.where == allocated::IN_REG)
        fprintf(out, "%s", X86::RegName(a->alloc.reg).c_str());
    else
        fprintf(out, "%d(%%ebp)", a->alloc.mem_offset);
}

void force_register(struct stack_allocation_mgr *s)
{
    s->stack[s->stack_top - 1]->alloc.where = allocated::IN_REG;
}
