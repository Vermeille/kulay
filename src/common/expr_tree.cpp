#include <stdio.h>
#include <common/opcodes.h>

#include <common/allocator.h>

typedef void (*dot_printer)(FILE*, struct ir_node*);

// declare dot nodes

void dot_declare(FILE *out, struct ir_node *n);

/*[[[cog
from opcodes import opcodes

for op in opcodes:
    cog.outl(
        'static void dot_declare_%s(FILE *out, struct ir_node *n)' % (op[0]))
    cog.outl('{')
    for n in [1, 2]:
        if op[n] == 'ARG':
            cog.outl('    dot_declare(out, n->arg%d);' % (n))
    cog.out('    fprintf(out, "%%d [label=\\"%%d: %s' % (op[0]))
    for n in [1, 2]:
        if op[n] == 'IARG':
            cog.out(' %x')
    cog.outl('\\"];\\n",')
    cog.outl('        n->index,')
    cog.outl('        n->index')
    for n in [1, 2]:
        if op[n] == 'IARG':
            cog.outl('        , n->iarg%d' % (n))
    cog.outl('    );')
    cog.outl('}')
    cog.outl('')
]]]*/
static void dot_declare_PUSH(FILE *out, struct ir_node *n)
{
    fprintf(out, "%d [label=\"%d: PUSH %x\"];\n",
        n->index,
        n->index
        , n->iarg1
    );
}

static void dot_declare_ADD(FILE *out, struct ir_node *n)
{
    dot_declare(out, n->arg1);
    dot_declare(out, n->arg2);
    fprintf(out, "%d [label=\"%d: ADD\"];\n",
        n->index,
        n->index
    );
}

static void dot_declare_SUB(FILE *out, struct ir_node *n)
{
    dot_declare(out, n->arg1);
    dot_declare(out, n->arg2);
    fprintf(out, "%d [label=\"%d: SUB\"];\n",
        n->index,
        n->index
    );
}

static void dot_declare_MUL(FILE *out, struct ir_node *n)
{
    dot_declare(out, n->arg1);
    dot_declare(out, n->arg2);
    fprintf(out, "%d [label=\"%d: MUL\"];\n",
        n->index,
        n->index
    );
}

static void dot_declare_EXIT(FILE *out, struct ir_node *n)
{
    dot_declare(out, n->arg1);
    fprintf(out, "%d [label=\"%d: EXIT\"];\n",
        n->index,
        n->index
    );
}

//[[[end]]]

static const dot_printer declarer[] = {
/*[[[cog
from opcodes import opcodes

for op in opcodes:
    cog.outl('    [%s] = dot_declare_%s,' % (op[0], op[0]))
]]]*/
[PUSH] = dot_declare_PUSH,
[ADD] = dot_declare_ADD,
[SUB] = dot_declare_SUB,
[MUL] = dot_declare_MUL,
[EXIT] = dot_declare_EXIT,
///[[[end]]]
};

void dot_declare(FILE *out, struct ir_node *n)
{
    declarer[n->op](out, n);
}

// describe graph

static void dot_print_(FILE *out, struct ir_node *n);

/*[[[cog
from opcodes import opcodes

for op in opcodes:
    cog.outl(
        'static void dot_print_%s(FILE *out, struct ir_node *n)' % (op[0]))
    cog.outl('{')
    cog.outl('    (void)out;')
    cog.outl('    (void)n;')
    for n in [1, 2]:
        if op[n] == 'ARG':
            cog.outl('    dot_print_(out, n->arg%d);' % (n))
            cog.outl('    fprintf(out, "%%d -> %%d [label=\\"",'
                ' n->index, n->arg%d->index);' % (n))
            cog.outl('    fprint_location(out, n->arg%d->alloc);' % n)
            cog.outl('    fprintf(out, "\\"];\\n");')
    cog.outl('}')
    cog.outl('')
]]]*/
static void dot_print_PUSH(FILE *out, struct ir_node *n)
{
    (void)out;
    (void)n;
}

static void dot_print_ADD(FILE *out, struct ir_node *n)
{
    (void)out;
    (void)n;
    dot_print_(out, n->arg1);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg1->index);
    fprint_location(out, n->arg1->alloc);
    fprintf(out, "\"];\n");
    dot_print_(out, n->arg2);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg2->index);
    fprint_location(out, n->arg2->alloc);
    fprintf(out, "\"];\n");
}

static void dot_print_SUB(FILE *out, struct ir_node *n)
{
    (void)out;
    (void)n;
    dot_print_(out, n->arg1);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg1->index);
    fprint_location(out, n->arg1->alloc);
    fprintf(out, "\"];\n");
    dot_print_(out, n->arg2);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg2->index);
    fprint_location(out, n->arg2->alloc);
    fprintf(out, "\"];\n");
}

static void dot_print_MUL(FILE *out, struct ir_node *n)
{
    (void)out;
    (void)n;
    dot_print_(out, n->arg1);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg1->index);
    fprint_location(out, n->arg1->alloc);
    fprintf(out, "\"];\n");
    dot_print_(out, n->arg2);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg2->index);
    fprint_location(out, n->arg2->alloc);
    fprintf(out, "\"];\n");
}

static void dot_print_EXIT(FILE *out, struct ir_node *n)
{
    (void)out;
    (void)n;
    dot_print_(out, n->arg1);
    fprintf(out, "%d -> %d [label=\"", n->index, n->arg1->index);
    fprint_location(out, n->arg1->alloc);
    fprintf(out, "\"];\n");
}

//[[[end]]]

static const dot_printer printer[] = {
/*[[[cog
from opcodes import opcodes

for op in opcodes:
    cog.outl('    [%s] = dot_print_%s,' % (op[0], op[0]))
]]]*/
[PUSH] = dot_print_PUSH,
[ADD] = dot_print_ADD,
[SUB] = dot_print_SUB,
[MUL] = dot_print_MUL,
[EXIT] = dot_print_EXIT,
//[[[end]]]
};

static void dot_print_(FILE *out, struct ir_node *n)
{
    printer[n->op](out, n);
}

void dot_print(struct ir_node *n)
{
    FILE *out = fopen("expr.dot", "w");
    fprintf(out, "digraph {\n");
    dot_declare(out, n);
    dot_print_(out, n);
    fprintf(out, "}\n");
    fclose(out);
}

