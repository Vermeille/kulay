#pragma once

#include <vector>

#include <common/opcodes.h>
#include <common/allocator.h>
#include <x86/emitter.h>

struct tile
{
    void (*codegen)(X86Emitter* x86, const struct ir_node *code);
    void (*liveness)(struct stack_allocation_mgr *s, struct ir_node *code);
    std::vector<Op*> pattern;
};

int find_tile(const struct ir_node *code);

extern const struct tile *tiles[];
