#include <common/var-analysis.h>

int VarAnalysis::GetNbVars() const
{
    int nb_vars = -1;

    for (auto& instr : code_)
        if (VarSet* v = dynamic_cast<VarSet*>(instr.get()))
            nb_vars = std::max(nb_vars, v->GetVarId());

    return nb_vars + 1;
}
