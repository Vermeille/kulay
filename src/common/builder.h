#pragma once

#include <common/opcodes.h>

#include <vector>
#include <memory>

class Code
{
    public:
        typedef std::vector<std::unique_ptr<Op>> ops_type;

        void Append(Op* op) { code_.push_back(std::unique_ptr<Op>(op)); }
        ops_type::iterator Begin() { return std::begin(code_); }
        ops_type::iterator End() { return std::end(code_); }
        ops_type::const_iterator Begin() const { return code_.cbegin(); }
        ops_type::const_iterator End() const { return code_.cend(); }
    private:
        std::vector<std::unique_ptr<Op>> code_;
};

Code::ops_type::iterator begin(Code& c);
Code::ops_type::iterator end(Code& c);

Code::ops_type::const_iterator begin(const Code& c);
Code::ops_type::const_iterator end(const Code& c);

class Builder
{
    public:
        Builder(const char* program);
        Code DoIt();
    private:
        const char* prgm_;
};
