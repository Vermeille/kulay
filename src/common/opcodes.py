opcodes = [
    ('PUSH', 'IARG',    'NOARG',  'NOJMP'),
    ('ADD',  'ARG',     'ARG',    'NOJMP'),
    ('SUB',  'ARG',     'ARG',    'NOJMP'),
    ('MUL',  'ARG',     'ARG',    'NOJMP'),
    ('EXIT', 'ARG',     'NOARG',  'JMP'),
]
