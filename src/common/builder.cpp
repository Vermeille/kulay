#include <stack>
#include <common/builder.h>

Code::ops_type::iterator begin(Code& c)
{
    return c.Begin();
}

Code::ops_type::iterator end(Code& c)
{
    return c.End();
}

Code::ops_type::const_iterator begin(const Code& c)
{
    return c.Begin();
}

Code::ops_type::const_iterator end(const Code& c)
{
    return c.End();
}

Builder::Builder(const char* program) : prgm_(program) {}

Code Builder::DoIt()
{
    Code code;
    std::stack<Op*> stack;
    size_t iter = 0;
    Op* op;
    while (prgm_[iter] != EXIT)
    {
        byte val = prgm_[iter];
        ++iter;
        switch (val)
        {
            case PUSH:
                op = new Push(*(int*)&prgm_[iter]);
                op->SetAddr(iter - 1);
                stack.push(op);
                code.Append(op);
                iter += 4;
                break;
            case ADD:
                op = new Add();
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                op->Arg(1) = stack.top();
                stack.pop();
                stack.push(op);
                code.Append(op);
                break;
            case SUB:
                op = new Sub();
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                op->Arg(1) = stack.top();
                stack.pop();
                stack.push(op);
                code.Append(op);
                break;
            case MUL:
                op = new Mul();
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                op->Arg(1) = stack.top();
                stack.pop();
                stack.push(op);
                code.Append(op);
                break;
            case VAR_GET:
                op = new VarGet(prgm_[iter]);
                op->SetAddr(iter - 1);
                stack.push(op);
                code.Append(op);
                ++iter;
                break;
            case VAR_SET:
                op = new VarSet(prgm_[iter]);
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                code.Append(op);
                ++iter;
                break;
            case EQ:
            case NEQ:
            case LT:
            case LTEQ:
            case GT:
            case GTEQ:
                op = new Cmp((opcode)val);
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                op->Arg(1) = stack.top();
                stack.pop();
                stack.push(op);
                code.Append(op);
                break;
            case IFNOT:
                op = new IfNot(*(unsigned int*)&prgm_[iter]);
                op->SetAddr(iter - 1);
                op->Arg(0) = stack.top();
                stack.pop();
                code.Append(op);
                iter += 4;
                break;
            case JMP:
                op = new Jmp(*(unsigned int*)&prgm_[iter]);
                op->SetAddr(iter - 1);
                code.Append(op);
                iter += 4;
                break;
        }
    }

    return code;
}
