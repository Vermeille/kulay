#pragma once

#include <common/opcodes.h>

#include <x86/arch.h>

//FIXME: rm this arbitraty value used for simplicity
#define STACK_LIVENESS_SIZE 256
#define MAX_LIVENESS (1024 * 1024)

struct ir_node;

struct allocated
{
    enum { UNALLOC, IN_REG, IN_MEM } where;
    union
    {
        int mem_offset;
        X86::Regs reg;
    };
};

struct liveness
{
    int definition;
    int last_use;
    struct allocated alloc;
};

struct stack_allocation_mgr
{
    struct liveness *stack[STACK_LIVENESS_SIZE];
    int stack_top;

    struct liveness *lives;
    int nbr_lives;

    X86::Regs free_regs[NBREGS];
    unsigned int remaining_regs;
    unsigned int mem_bitset;
};

void init_stack_allocator(struct stack_allocation_mgr *s);

struct liveness *push_tmp(struct stack_allocation_mgr *stack, int here);
struct liveness *use_tmp(struct stack_allocation_mgr *stack,int offset, int here);
struct liveness *use_top_tmp(struct stack_allocation_mgr *stack, int here);
struct liveness *pop_tmp(struct stack_allocation_mgr *stack, int here);

void liveness_analysis(struct stack_allocation_mgr *s, struct ir_node *code);

void force_register(struct stack_allocation_mgr *stack);
void print_location(const struct liveness *a);
void fprint_location(FILE *out, const struct liveness *a);
