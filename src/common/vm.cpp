#include <common/vm.h>
#include <common/opcodes.h>
#include <common/builder.h>
#include <common/var-analysis.h>
#include <iostream>

void VM::Interpret()
{
    {
        Builder b(program_);
        Code c = b.DoIt();
        VarAnalysis va(c);
        Reserve(va.GetNbVars());
    }

    while (program_[pc_] != EXIT)
    {
        byte instr = program_[pc_];
        ++pc_;
        switch (instr)
        {
            case PUSH:
                Push::Interpret(this);
                break;
            case ADD:
                Add::Interpret(this);
                break;
            case SUB:
                Sub::Interpret(this);
                break;
            case MUL:
                Mul::Interpret(this);
                break;
            case VAR_GET:
                VarGet::Interpret(this);
                break;
            case VAR_SET:
                VarSet::Interpret(this);
                break;
            case EXIT:
                break;
            case EQ:
            case NEQ:
            case LT:
            case LTEQ:
            case GT:
            case GTEQ:
                Cmp::Interpret((opcode)instr, this);
                break;
            case IFNOT:
                IfNot::Interpret(this);
                break;
            case JMP:
                Jmp::Interpret(this);
                break;
        }
    }
    printf("int: %d\n", *(sp_ - 1));
}

void VM::SimpleCompile()
{
    while (program_[pc_] != EXIT)
    {
        x86_.DeclareBCAddr(pc_);
        byte val = program_[pc_];
        ++pc_;
        switch (val)
        {
            case PUSH:
                Push::SimpleCompile(ReadInt(), &x86_);
                AdvancePc(4);
                break;
            case ADD:
                Add::SimpleCompile(&x86_);
                break;
            case SUB:
                Sub::SimpleCompile(&x86_);
                break;
            case MUL:
                Mul::SimpleCompile(&x86_);
                break;
            case VAR_GET:
                VarGet::SimpleCompile(ReadByte(), &x86_);
                AdvancePc(1);
                break;
            case VAR_SET:
                VarSet::SimpleCompile(ReadByte(), &x86_);
                AdvancePc(1);
                break;
            case EQ:
            case NEQ:
            case LT:
            case LTEQ:
            case GT:
            case GTEQ:
                Cmp::SimpleCompile((opcode)val, &x86_);
                break;
            case IFNOT:
                IfNot::SimpleCompile(ReadInt(), &x86_);
                AdvancePc(4);
                break;
            case JMP:
                Jmp::SimpleCompile(ReadInt(), &x86_);
                AdvancePc(4);
                break;
        }
    }
    x86_.pop(X86::eax);
    x86_.ret();

    int a = x86_.Compile()();
    printf("aot: %d\n", a);
}

void VM::Compile()
{
    Builder b(program_);
    Code c = b.DoIt();

    VarAnalysis va(c);

    int locals_offset = sizeof (int) * (va.GetNbVars() + 1);

    x86_.push(X86::ebp);
    x86_.mov(Operand(X86::esp), X86::ebp);
    x86_.sub(locals_offset, Operand(X86::esp));

    for (auto& instr : c)
    {
        x86_.DeclareBCAddr(instr->GetAddr());
        instr->Codegen(&x86_);
    }

    x86_.pop(X86::eax);
    x86_.add(locals_offset, Operand(X86::esp));
    x86_.pop(X86::ebp);
    x86_.ret();

    auto res = x86_.Compile();
    int a = res();
    printf("%d\n", a);
}
