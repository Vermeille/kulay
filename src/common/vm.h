#pragma once

#include <vector>
#include <x86/emitter.h>

class VM
{
    public:
        VM(const char* program) : pc_(0), sp_(stack_), bp_(sp_), program_(program) {}
        void AdvancePc(int disp = 1) { pc_ += disp; }
        void SetPc(unsigned where) { pc_ = where; }

        void Interpret();
        void SimpleCompile();
        void Compile();
        int& StackAt(int offset) { return sp_ [-(offset + 1)]; }

        void Push(int val)
        {
            *sp_ = val;
            ++sp_;
        }

        void Reserve(int sz) { sp_ += sz; }

        int Pop()
        {
            int val = sp_[-1];
            --sp_;
            return val;
        }

        int& GetLocal(int idx) { return bp_[idx]; } // FIXME: add +1 when funcalls will arrive

        int ReadInt() const { return *(int*)&program_[pc_]; }
        int ReadByte() const { return program_[pc_]; }

    private:
        int stack_[1024];
        unsigned int pc_;
        int* sp_;
        int* bp_;
        const char* program_;
        X86Emitter x86_;
};
