#include <assert.h>
#include <common/linear_scan.h>

#include <x86/arch.h>

static
void rm_active(int *active, int *nb_active, int key)
{
    for (int i = key; i < *nb_active - 1; ++i)
    {
        active[i] = active[i + 1];
    }
    --*nb_active;
}

static
struct allocated regalloc(struct stack_allocation_mgr* s)
{
    struct allocated res;
    if (s->remaining_regs != 0)
    {
        --s->remaining_regs;
        res.reg = s->free_regs[s->remaining_regs];
        res.where = allocated::IN_REG;
        return res;
    }

    if (s->mem_bitset == 0)
    {
        fprintf(stderr, "cannot allocate that much!\n");
        exit(1);
    }

    for (int i = 0; i < 32; ++i)
        if (s->mem_bitset & (1 << i))
        {
            s->mem_bitset &= ~(1 << i);
            res.where = allocated::IN_MEM;
            res.mem_offset = -(i * 4 + 4);
            printf("bit %d allocated\n", i);
            return res;
        }
    fprintf(stderr, "you can't reach here\n");
    exit(1);
}

static
void regfree(struct stack_allocation_mgr *s, struct allocated *a)
{
    if (a->where == allocated::IN_REG)
    {
        printf("free reg %s\n", X86::RegName(a->reg).c_str());
        s->free_regs[s->remaining_regs] = a->reg;
        ++s->remaining_regs;
    }
    else
    {
        s->mem_bitset |= 1 << ((-a->mem_offset - 4) / 4);
    }
}

static
void expire_active(int *active, int *nb_active,
        struct stack_allocation_mgr *s, int now)
{
    for (int j = 0; j < *nb_active; ++j)
    {
        if (s->lives[active[j]].last_use <= now)
        {
            regfree(s, &s->lives[active[j]].alloc);
            rm_active(active, nb_active, j);
            --j;
        }
    }
}

/**
 * @brief Spills either `life` either a variable from reg_active, moving it to
 * mem_active.
 */
static
void spill(int *reg_active, int *nb_reg_active, int *mem_active, int *nb_mem_active,
        struct stack_allocation_mgr *s, int life)
{
    int spill_idx = -1;
    int max = 0;
    struct liveness *spill;
    for (int i = 0; i < *nb_reg_active; ++i)
    {
        struct liveness *spill_iterator = &s->lives[reg_active[i]];
        printf("check spill of %s (%d -> %d)\n", X86::RegName(spill_iterator->alloc.reg).c_str(), spill_iterator->definition, spill_iterator->last_use);
        if (spill_iterator->last_use > max)
        {
            spill_idx = i;
            spill = spill_iterator;
            max = spill_iterator->last_use;
        }
    }
    struct liveness *cur = &s->lives[life];
    if (spill->last_use > cur->last_use)
    {
        cur->alloc = spill->alloc;
        spill->alloc = regalloc(s);
        printf("spill %s ", X86::RegName(cur->alloc.reg).c_str());
        printf("at %d(%%rsp)\n", spill->alloc.mem_offset);
        mem_active[(*nb_mem_active)++] = reg_active[spill_idx];
        rm_active(reg_active, nb_reg_active, spill_idx);

        reg_active[*nb_reg_active] = life;
        ++*nb_reg_active;
    }
    else
    {
        cur->alloc = regalloc(s);
        mem_active[(*nb_mem_active)++] = life;
    }
}

void allocate(struct stack_allocation_mgr *s)
{
    int reg_active[NBREGS];
    int nb_reg_active = 0;

    int mem_active[256];
    int nb_mem_active = 0;

    int regforce_active[NBREGS];
    int nb_regforce_active = 0;

    for (int i = 0; i < s->nbr_lives; ++i)
    {
        struct liveness *cur = &s->lives[i];

        printf("%d -> %d [%s] | f: %d r: %d\n", cur->definition, cur->last_use, (cur->alloc.where == allocated::UNALLOC) ? "UNALLOC" : "PREALLOC", nb_regforce_active, nb_reg_active);
        assert(nb_reg_active + nb_regforce_active <= NBREGS);
        expire_active(mem_active, &nb_mem_active, s, cur->definition);
        expire_active(reg_active, &nb_reg_active, s, cur->definition);
        expire_active(regforce_active, &nb_regforce_active, s, cur->definition);

        if (nb_reg_active + nb_regforce_active == NBREGS)
        {
            if (cur->alloc.where == allocated::UNALLOC)
            {
                spill(reg_active, &nb_reg_active, mem_active, &nb_mem_active, s, i);
            }
            else
            {
                struct liveness *spill = &s->lives[reg_active[nb_reg_active - 1]];

                cur->alloc = spill->alloc;
                spill->alloc = regalloc(s);

                mem_active[nb_mem_active++] = reg_active[nb_reg_active - 1];
                rm_active(reg_active, &nb_reg_active, nb_reg_active - 1);

                regforce_active[nb_regforce_active++] = i;
            }
        }
        else
        {
            if (cur->alloc.where == allocated::UNALLOC)
            {
                cur->alloc = regalloc(s);
                printf("allocate %s\n", X86::RegName(cur->alloc.reg).c_str());
                reg_active[nb_reg_active++] = i;
            }
            else
            {
                cur->alloc = regalloc(s);
                regforce_active[nb_regforce_active++] = i;
            }
        }
    }
}

