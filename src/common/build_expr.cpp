#include <stdlib.h>
#include <common/opcodes.h>

struct op_stack
{
    struct ir_node *ops[256];
    int sz;
};


struct ir_node *make_expr_tree(const byte *prgm, unsigned int *rdptr)
{
    struct op_stack stack;
    stack.sz = 0;

    struct ir_node *tmp;
    while (1)
    {
        switch (prgm[*rdptr])
        {
/*[[[cog
from opcodes import opcodes

for op in  opcodes:
    cog.outl('case %s:' % op[0])
    cog.outl('    tmp = (ir_node*)malloc(sizeof (*tmp));')
    cog.outl('    tmp->op = %s;' % op[0])
    cog.outl('    tmp->index = *rdptr * 10;')
    cog.outl('    tmp->alloc = NULL;')
    for n in [2, 1]:
        if op[n] == 'ARG':
            cog.outl('    tmp->arg%d = stack.ops[stack.sz -1];' % n)
            cog.outl('    --stack.sz;')
        elif op[n] == 'IARG':
            cog.outl('    tmp->iarg%d = *(int*)(prgm + *rdptr + 1);' % n)
            cog.outl('    *rdptr += 4;')
    cog.outl('    stack.ops[stack.sz] = tmp;')
    cog.outl('    ++stack.sz;')
    if op[3] == 'NOJMP':
        cog.outl('    break;')
    else:
        cog.outl('    return stack.ops[0];')
]]]*/
case PUSH:
    tmp = (ir_node*)malloc(sizeof (*tmp));
    tmp->op = PUSH;
    tmp->index = *rdptr * 10;
    tmp->alloc = NULL;
    tmp->iarg1 = *(int*)(prgm + *rdptr + 1);
    *rdptr += 4;
    stack.ops[stack.sz] = tmp;
    ++stack.sz;
    break;
case ADD:
    tmp = (ir_node*)malloc(sizeof (*tmp));
    tmp->op = ADD;
    tmp->index = *rdptr * 10;
    tmp->alloc = NULL;
    tmp->arg2 = stack.ops[stack.sz -1];
    --stack.sz;
    tmp->arg1 = stack.ops[stack.sz -1];
    --stack.sz;
    stack.ops[stack.sz] = tmp;
    ++stack.sz;
    break;
case SUB:
    tmp = (ir_node*)malloc(sizeof (*tmp));
    tmp->op = SUB;
    tmp->index = *rdptr * 10;
    tmp->alloc = NULL;
    tmp->arg2 = stack.ops[stack.sz -1];
    --stack.sz;
    tmp->arg1 = stack.ops[stack.sz -1];
    --stack.sz;
    stack.ops[stack.sz] = tmp;
    ++stack.sz;
    break;
case MUL:
    tmp = (ir_node*)malloc(sizeof (*tmp));
    tmp->op = MUL;
    tmp->index = *rdptr * 10;
    tmp->alloc = NULL;
    tmp->arg2 = stack.ops[stack.sz -1];
    --stack.sz;
    tmp->arg1 = stack.ops[stack.sz -1];
    --stack.sz;
    stack.ops[stack.sz] = tmp;
    ++stack.sz;
    break;
case EXIT:
    tmp = (ir_node*)malloc(sizeof (*tmp));
    tmp->op = EXIT;
    tmp->index = *rdptr * 10;
    tmp->alloc = NULL;
    tmp->arg1 = stack.ops[stack.sz -1];
    --stack.sz;
    stack.ops[stack.sz] = tmp;
    ++stack.sz;
    return stack.ops[0];
//[[[end]]]
default:
    fprintf(stderr, "this opcode does not exists\n");
        }
        ++*rdptr;
    }
}
