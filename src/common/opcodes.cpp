#include <common/opcodes.h>
#include <common/vm.h>
#include <x86/emitter.h>

void Push::Interpret(VM* vm)
{
    vm->Push(vm->ReadInt());
    vm->AdvancePc(4);
}

void Push::SimpleCompile(int val, X86Emitter* x86)
{
    x86->push(val);
}

void Pop::Interpret(VM* vm) { vm->Pop(); }
void Pop::SimpleCompile(X86Emitter* x86) { x86->pop(X86::eax); }

void Add::Interpret(VM* vm)
{
    vm->Push(vm->Pop() + vm->Pop());
}

void Add::SimpleCompile(X86Emitter* x86)
{
    x86->pop(X86::eax);
    x86->pop(X86::ebx);
    x86->add(Operand(X86::eax), X86::ebx);
    x86->push(Operand(X86::ebx));
}

void Sub::Interpret(VM* vm)
{
    int a = vm->Pop();
    int b = vm->Pop();
    vm->Push(b - a);
}

void Sub::SimpleCompile(X86Emitter* x86)
{
    x86->pop(X86::eax);
    x86->pop(X86::ebx);
    x86->sub(Operand(X86::eax), X86::ebx);
    x86->push(Operand(X86::ebx));
}

void Mul::Interpret(VM* vm)
{
    vm->Push(vm->Pop() * vm->Pop());
}

void Mul::SimpleCompile(X86Emitter* x86)
{
    x86->pop(X86::eax);
    x86->pop(X86::ebx);
    x86->mul(Operand(X86::eax), X86::ebx);
    x86->push(Operand(X86::ebx));
}

void VarGet::Interpret(VM* vm)
{
    vm->Push(vm->GetLocal(vm->ReadByte()));
    vm->AdvancePc(1);
}

void VarGet::SimpleCompile(int idx, X86Emitter* x86)
{
    x86->push(Operand(X86::ebp, x86->GetLocalVarOffset(idx)));
}

void VarSet::Interpret(VM* vm)
{
    vm->GetLocal(vm->ReadByte()) = vm->Pop();;
    vm->AdvancePc(1);
}

void VarSet::SimpleCompile(int idx, X86Emitter* x86)
{
    // TODO: factorize
    x86->pop(X86::eax);
    x86->mov(X86::eax, Operand(X86::ebp, x86->GetLocalVarOffset(idx)));
}

void Cmp::Interpret(opcode cmp, VM* vm)
{
    int b = vm->Pop();
    int a = vm->Pop();
    switch (cmp)
    {
        case EQ:   vm->Push(a == b); break;
        case NEQ:  vm->Push(a != b); break;
        case LT:   vm->Push(a < b); break;
        case LTEQ: vm->Push(a <= b); break;
        case GT:   vm->Push(a > b); break;
        case GTEQ: vm->Push(a >= b); break;
    }
}

void Cmp::SimpleCompile(opcode cmp, X86Emitter* x86)
{
    x86->pop(X86::ebx);
    x86->pop(X86::eax);
    x86->cmp(Operand(X86::eax), X86::ebx);
    switch (cmp)
    {
        case EQ:   x86->sete(X86::eax); break;
        case NEQ:  x86->setne(X86::eax); break;
        case LT:   x86->setl(X86::eax); break;
        case LTEQ: x86->setle(X86::eax); break;
        case GT:   x86->setg(X86::eax); break;
        case GTEQ: x86->setge(X86::eax); break;
    }

    x86->movzb(Operand(X86::eax), X86::eax);
    x86->push(X86::eax);
}

void IfNot::Interpret(VM* vm)
{
    int cond = vm->Pop();
    if (!cond)
        vm->SetPc(vm->ReadInt());
    else
        vm->AdvancePc(4);
}

void IfNot::SimpleCompile(unsigned int addr, X86Emitter* x86)
{
    x86->pop(X86::eax);
    x86->And(Operand(X86::eax), X86::eax);
    x86->jne(addr);
}

void Jmp::Interpret(VM* vm)
{
    vm->SetPc(vm->ReadInt());
}

void Jmp::SimpleCompile(unsigned int addr, X86Emitter* x86)
{
    x86->jmp(addr);
}
