#pragma once

#include <cassert>
#include <vector>
#include <stdint.h>
#include <common/allocator.h>

#include <x86/arch.h>

class X86Emitter;

enum opcode
{
    PUSH,
    ADD,
    SUB,
    MUL,
    EXIT,
    VAR_GET,
    VAR_SET,
    POP,
    EQ,
    NEQ,
    LT,
    LTEQ,
    GT,
    GTEQ,
    IFNOT,
    JMP,
};

class Location
{
    private:
        enum LocType { IN_REG, IN_MEM, UNALLOC } where_;
        union
        {
            int mem_offset_;
            X86::Regs reg_;
        };

    public:
        Location() : where_(UNALLOC), mem_offset_(-1) {}
        LocType Where() const { return where_; }
        int GetMemOffset() const { assert(where_ == IN_MEM); return mem_offset_; }
        X86::Regs GetReg() const { assert(where_ == IN_REG); return reg_; }
        void MarkOnStack() { assert(where_ == UNALLOC); where_ = IN_MEM; }
        void MarkInReg() { assert(where_ == UNALLOC); where_ = IN_REG; }
        void AsStackOffset(int off)
        {
            assert(where_ == UNALLOC || where_ == IN_MEM);
            where_ = IN_MEM;
            mem_offset_ = off;
        }
        void AsReg(X86::Regs r)
        {
            assert(where_ == UNALLOC || where_ == IN_REG);
            where_ = IN_REG;
            reg_ = r;
        }
};

class VM;

class Op
{
    public:
        Op*& Arg(int idx) { return args_[idx]; }

        size_t NbArgs() const { return args_.size(); }

        virtual void Codegen(X86Emitter* x86) = 0;

        const Location& GetLocation() const { return loc_; }
        void RequireInMem() { loc_.MarkOnStack(); }
        void RequireRegister() { loc_.MarkInReg(); }
        void RequireRegister(X86::Regs r) { loc_.AsReg(r); }

        virtual bool IsOp() const { return true; }
        virtual bool IsPush() const { return false; }
        virtual bool IsAdd() const { return false; }
        virtual bool IsSub() const { return false; }
        virtual bool IsMul() const { return false; }

        virtual std::string ToString() const = 0;

        void SetAddr(unsigned int addr) { addr_ = addr; }
        unsigned int GetAddr() const { return addr_; }

    protected:
        Op(int nb_args) : args_(nb_args) {}

    private:
        std::vector<Op*> args_;
        Location loc_;
        unsigned int addr_;
};

class Push : public Op
{
    public:
        Push(int val) : Op(0), val_(val) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(int val, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(val_, x86); }
        int GetValue() const { return val_; }
        virtual std::string ToString() const override { return "Push " + std::to_string(val_); }

    private:
        int val_;
};

class Pop : public Op
{
    static void Interpret(VM* vm);
    // FIXME: Do not waste a register
    static void SimpleCompile(X86Emitter* x86);
    virtual void Codegen(X86Emitter* x86) override { SimpleCompile(x86); }
};

class Add : public Op
{
    public:
        Add() : Op(2) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(x86); }
        virtual bool IsAdd() const override { return true; }
        virtual std::string ToString() const override { return "Add"; }
};

class Sub : public Op
{
    public:
        Sub() : Op(2) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(x86); };
        virtual bool IsSub() const override { return true; }
        virtual std::string ToString() const override { return "Sub"; }
};

class Mul : public Op
{
    public:
        Mul() : Op(2) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(x86); }
        virtual bool IsMul() const override { return true; }
        virtual std::string ToString() const override { return "Mul"; }
};

class VarGet : public Op
{
    public:
        VarGet(int var_id) : Op(0), var_id_(var_id) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(int idx, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(var_id_, x86); }
        int GetVarId() const { return var_id_; }
        virtual std::string ToString() const override { return "VarGet " + std::to_string(var_id_); }
    private:
        int var_id_;
};

class VarSet : public Op
{
    public:
        VarSet(int var_id) : Op(1), var_id_(var_id) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(int idx, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(var_id_, x86); }
        int GetVarId() const { return var_id_; }
        virtual std::string ToString() const override { return "VarSet " + std::to_string(var_id_); }
    private:
        int var_id_;
};

class Cmp : public Op
{
    public:
        Cmp(opcode op) : Op(2), op_(op) {}
        static void Interpret(opcode cmp, VM* vm);
        static void SimpleCompile(opcode cmp, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(op_, x86); }
        virtual std::string ToString() const override { return "Cmp"; }

    private:
        opcode op_;
};

class IfNot : public Op
{
    public:
        IfNot(unsigned int dst) : Op(1), dst_(dst) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(unsigned int dst, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(dst_, x86); }
        virtual std::string ToString() const override { return "IfNot"; }

    private:
        unsigned int dst_;
};

class Jmp : public Op
{
    public:
        Jmp(unsigned int dst) : Op(0), dst_(dst) {}
        static void Interpret(VM* vm);
        static void SimpleCompile(unsigned int dst, X86Emitter* x86);
        virtual void Codegen(X86Emitter* x86) override { SimpleCompile(dst_, x86); }
        virtual std::string ToString() const override { return "Jmp"; }

    private:
        unsigned int dst_;
};

struct ir_node
{
    enum opcode op;

    union {
        struct ir_node *arg1;
        int iarg1;
        uint32_t uarg1;
    };

    union {
        struct ir_node *arg2;
        int iarg2;
        uint32_t uarg2;
    };

    int index;
    struct liveness *alloc;
};

typedef unsigned char byte;

