#include <common/builder.h>

class VarAnalysis
{
    public:
        VarAnalysis(const Code& code) : code_(code) {}
        int GetNbVars() const;

    private:
        const Code& code_;
};
