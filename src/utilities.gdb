define indent
    set $val = $arg0
    while $val
        printf " "
        set $val = $val - 1
    end
end

define instrs
    if $arg0
        instrs $arg0->arg1 $arg1+1
        indent $arg1
        printf "%3d: ", $arg0->index
        output (enum opcode)prgm[$arg0->index]
        printf "\n"
        instrs $arg0->arg2 $arg1+1
    end
end

b 78
r prgm.bin
instrs stack.ops[0] 0
