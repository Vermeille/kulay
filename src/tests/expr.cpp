#include <iostream>
#include <cstdlib>
#include <ctime>

void print_expr(int nb_vars)
{
    switch (rand() % 4)
    {
        case 0:
            std::cout << rand() % 65535;
            break;
        case 1:
            {
                print_expr(nb_vars);
                switch (rand() % 3)
                {
                    case 0:
                        std::cout << " + ";
                        break;
                    case 1:
                        std::cout << " - ";
                        break;
                    case 2:
                        std::cout << " * ";
                        break;
                }
                print_expr(nb_vars);
            }
            break;
        case 2:
            std::cout << "(";
            print_expr(nb_vars);
            std::cout << ")";
            break;
        case 3:
            if (!nb_vars)
                print_expr(nb_vars);
            else
                std::cout << static_cast<char>('a' + rand() % nb_vars);
            break;
    }
}

void def_var(int& nb_vars)
{
    if (!nb_vars)
    {
        std::cout << "a = ";
        print_expr(nb_vars);
        ++nb_vars;
        std::cout << ";\n";
        return;
    }
    else if (nb_vars == 26)
    {
        std::cout << static_cast<char>('a' + rand() % nb_vars) << " = ";
        print_expr(nb_vars);
        std::cout << ";\n";
        return;
    }

    switch (rand() % 2)
    {
        case 0:
            std::cout << static_cast<char>('a' + rand() % nb_vars) << " = ";
            print_expr(nb_vars);
            break;
        case 1:
            std::cout << static_cast<char>('a' + nb_vars) << " = ";
            print_expr(nb_vars);
            ++nb_vars;
            break;
    }
    std::cout << ";\n";
}

void print_cond(int nb_vars)
{
    print_expr(nb_vars);
    switch (rand() % 6)
    {
        case 0: std::cout << " < "; break;
        case 1: std::cout << " <= "; break;
        case 2: std::cout << " > "; break;
        case 3: std::cout << " >= "; break;
        case 4: std::cout << " == "; break;
        case 5: std::cout << " != "; break;
    }
    print_expr(nb_vars);
}

void ident(int level)
{
    while (level-- > 0)
    {
        std::cout << "  ";
    }
}

void print_block(int& nb_vars, int level)
{
    ident(level - 1);
    std::cout << "{\n";
    int cont = true;
    do
    {
        switch (rand() % 3)
        {
            case 0:
                ident(level);
                std::cout << "if ";
                print_cond(nb_vars);
                std::cout << "\n";
                print_block(nb_vars, level + 1);
                break;
            case 1:
                ident(level);
                def_var(nb_vars);
                break;
            case 2:
                cont = false;
                if (level == 1)
                {
                    ident(level);
                    std::cout << "return ";
                    print_expr(nb_vars);
                    std::cout << ";\n";
                }
                break;
        }
    } while (cont);
    ident(level - 1);
    std::cout << "}\n";
}

int main()
{
    srand(clock());
    int nb_vars = 0;
    print_block(nb_vars, 1);
    return 0;
}
