#include "genrand.h"

#include <stdio.h>
#include <stdlib.h>

static const regs gpregsenum[] =
        { X86::eax, X86::edx, X86::ecx, X86::ebx, X86::edi, X86::esi, X86::esp, X86::ebp };

regs gen_reg()
{
    return gpregsenum[rand() % 8];
}

template <int>
const char *regstr(regs r)
{
    switch (r)
    {
        case X86::eax:
            return "eax";
        case X86::edx:
            return "edx";
        case X86::ecx:
            return "ecx";
        case X86::ebx:
            return "ebx";
        case X86::edi:
            return "edi";
        case X86::esi:
            return "esi";
        case X86::esp:
            return "esp";
        case X86::ebp:
            return "ebp";
        default:
            return "error";
    }
}

template <>
const char *regstr<8>(regs r)
{
    switch (r)
    {
        case X86::eax:
            return "al";
        case X86::edx:
            return "dl";
        case X86::ecx:
            return "cl";
        case X86::ebx:
            return "bl";
        case X86::edi:
            return "bh";
        case X86::esi:
            return "dh";
        case X86::esp:
            return "ah";
        case X86::ebp:
            return "ch";
        default:
            return "error";
    }
}

Operand gen_op(FILE *sfile)
{
    regs reg = (X86::Regs)gen_reg();
    switch (rand() % 2)
    {
    case 0:
        return Operand(reg);
    case 1:
        {
            int d = (rand() % 1024) - 512;
            return Operand(reg, d);
        }
    default:
        return Operand(X86::ebp); // dummy
    }
}

template <int N>
void print_op(FILE *sfile, const Operand *o)
{
    if (!o->is_mem)
    {
        fprintf(sfile, "%%%s", regstr<N>(o->base));
    }
    else
    {
        if (o->index == X86::noreg)
        {
            if (o->scale > 1)
                fprintf(sfile, "%d(%%%s, %%%s, %d)", o->disp, regstr<32>(o->base),
                        regstr<32>(o->index), o->scale);
            else
                fprintf(sfile, "%d(%%%s)", o->disp, regstr<32>(o->base));
        }
    }
}

typedef void (X86Emitter::*fun_r)(regs);

template <int N = 32>
void test_r(X86Emitter* x86, const char *op_name, FILE *sfile, fun_r jit)
{
    regs r = gen_reg();
    (x86->*jit)(r);
    fprintf(sfile, "%s %%%s", op_name, regstr<N>(r));
    fprintf(sfile, "\n");
}

typedef void (X86Emitter::*fun_o_r)(Operand, regs);

template <int N1 = 32, int N2 = 32>
void test_o_r(X86Emitter* x86, const char *op_name, FILE *sfile, fun_o_r jit)
{
    regs r = gen_reg();
    Operand o = gen_op(sfile);
    (x86->*jit)(o, r);
    fprintf(sfile, "%s ", op_name);
    print_op<N1>(sfile, &o);
    fprintf(sfile, ", %%%s\n", regstr<N2>(r));
}

typedef void (X86Emitter::*fun_r_o)(regs, Operand);

template <int N1 = 32, int N2 = 32>
void test_r_o(X86Emitter* x86, const char *op_name, FILE *sfile, fun_r_o jit)
{
    regs r = gen_reg();
    Operand o = gen_op(sfile);
    (x86->*jit)(r, o);
    fprintf(sfile, "%s %%%s, ", op_name, regstr<N1>(r));
    print_op<N2>(sfile, &o);
    fprintf(sfile, "\n");
}

typedef void (X86Emitter::*fun_i_o)(int, Operand);

template <int N = 32>
void test_i_o(X86Emitter* x86, const char *op_name, FILE *sfile, fun_i_o jit)
{
    Operand o = gen_op(sfile);
    int imm = rand() & 0x7FFFFFFF;
    (x86->*jit)(imm, o);

    if (o.is_mem)
        fprintf(sfile, "%sl $%d, ", op_name, imm);
    else
        fprintf(sfile, "%s $%d, ", op_name, imm);
    print_op<N>(sfile, &o);
    fprintf(sfile, "\n");
}

typedef void (X86Emitter::*fun_i_r)(int, regs);

template <int N = 32>
void test_i_r(X86Emitter* x86, const char *op_name, FILE *sfile, fun_i_r jit)
{
    regs o = gen_reg();
    int imm = rand() & 0x7FFFFFFF;
    (x86->*jit)(imm, o);

    fprintf(sfile, "%s $%d, %%%s\n", op_name, imm, regstr<N>(o));
}

typedef void (X86Emitter::*fun_i_o_r)(int, Operand, regs);

template <int N = 32, int N2 = 32>
void test_i_o_r(X86Emitter* x86, const char *op_name, FILE *sfile, fun_i_o_r jit)
{
    Operand o = gen_op(sfile);
    regs r = gen_reg();
    int imm = rand() & 0x7FFFFFFF;
    (x86->*jit)(imm, o, r);

    fprintf(sfile, "%s $%d, ", op_name, imm);
    print_op<N>(sfile, &o);
    fprintf(sfile, ", %%%s\n", regstr<N2>(r));
}

int main()
{
    FILE* sfile = fopen("/tmp/out.s", "w");
    FILE* bin = fopen("/tmp/emitted.bin", "w");
    unsigned char buffer[1024*1024];
    unsigned char *iter = buffer;
    srand(clock());
    X86Emitter x86(buffer);

    for (int i = 0; i < 10; ++i)
    {
        test_o_r(&x86, "add", sfile, &X86Emitter::add);
        test_r_o(&x86, "add", sfile, &X86Emitter::add);
        test_i_o(&x86, "add", sfile, &X86Emitter::add);

        test_o_r(&x86, "sub", sfile, &X86Emitter::sub);
        test_r_o(&x86, "sub", sfile, &X86Emitter::sub);
        test_i_o(&x86, "sub", sfile, &X86Emitter::sub);

        test_o_r(&x86, "mov", sfile, &X86Emitter::mov);
        test_r_o(&x86, "mov", sfile, &X86Emitter::mov);
        test_i_o(&x86, "mov", sfile, &X86Emitter::mov);

        test_o_r(&x86, "imul", sfile, &X86Emitter::mul);
        test_i_r(&x86, "imul", sfile, &X86Emitter::mul);
        test_i_o_r(&x86, "imul", sfile, &X86Emitter::mul);

        test_r<8>(&x86, "setl", sfile, &X86Emitter::setl);
        test_r<8>(&x86, "setle", sfile, &X86Emitter::setle);
        test_r<8>(&x86, "setg", sfile, &X86Emitter::setg);
        test_r<8>(&x86, "setge", sfile, &X86Emitter::setge);
        test_r<8>(&x86, "sete", sfile, &X86Emitter::sete);
        test_r<8>(&x86, "setne", sfile, &X86Emitter::setne);

        test_o_r(&x86, "cmp", sfile, &X86Emitter::cmp);
        test_r_o(&x86, "cmp", sfile, &X86Emitter::cmp);

        test_o_r<8, 32>(&x86, "movzbl", sfile, &X86Emitter::movzb);
    }

    fwrite(buffer, x86.SizeSoFar(), 1, bin);
    fclose(sfile);
    fclose(bin);

    int ret = system(
            "cd /tmp; "
            //"cat out.s && "
            "gcc -m32 -c out.s -o ref.o && "
            "ld -melf_i386 ref.o -o ref --oformat=binary 2> /dev/null && "
            "objdump --no-show-raw-insn -D -b binary -mi386 emitted.bin | tail -n +8 | cut -d: -f 2- > a && "
            "objdump --no-show-raw-insn -D -b binary -mi386 ref | tail -n +8 | cut -d: -f 2- > b && "
            "diff a b");

    return ret ? 1 : 0;
}

