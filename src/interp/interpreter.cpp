#include <stdio.h>
#include <common/opcodes.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdint.h>
#include <common/vm.h>

int main(int argc, char **argv)
{
    if (argc < 2)
        return 1;

    struct stat sb;
    char *program;

    int fd = open(argv[1], O_RDONLY);
    fstat(fd, &sb);

    program = (char*)mmap(NULL, sb.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0);

    VM v(program);
    v.Interpret();

    return 0;
}
