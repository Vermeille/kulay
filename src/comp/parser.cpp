#include "parser.h"

#include <cctype>
#include <memory>

std::unique_ptr<Exp> parse_cmp(const Lexes& lexes, int& iter);

std::unique_ptr<Exp> parse_term(const Lexes& lexes, int& iter)
{
    bool neg = false;
    std::unique_ptr<Exp> res;
    if (lexes[iter] == "-")
    {
        neg = true;
        ++iter;
    }

    if (lexes[iter] == "(")
    {
        ++iter;
        res = parse_cmp(lexes, iter);
        if (lexes[iter] != ")")
            return nullptr;
        ++iter;
    }
    else if (isdigit(lexes[iter][0]))
    {
        res = std::unique_ptr<Exp>(new IntLit(std::atoi(lexes[iter].c_str())));
        ++iter;
    }
    else
    {
        res = std::unique_ptr<Exp>(new VarGet(lexes[iter]));;
        ++iter;
    }

    if (neg)
    {
        Neg* n = new Neg;
        n->e_ = std::move(res);
        res = std::unique_ptr<Exp>(n);
    }
    return res;
}

std::unique_ptr<Exp> parse_mul(const Lexes& lexes, int& iter)
{
    auto lhs = parse_term(lexes, iter);

    while (lexes[iter] == "*" || lexes[iter] == "/")
    {
        std::string op = lexes[iter++];
        auto rhs = parse_mul(lexes, iter);
        std::unique_ptr<Binop> tmp(new Binop);
        tmp->op_ = (op == "*") ? MUL : DIV;
        tmp->lhs_ = std::move(lhs);
        tmp->rhs_ = std::move(rhs);
        lhs = std::move(tmp);
    }
    return lhs;
}

std::unique_ptr<Exp> parse_add(const Lexes& lexes, int& iter)
{
    auto lhs = parse_mul(lexes, iter);

    while (lexes[iter] == "+" || lexes[iter] == "-")
    {
        std::string op = lexes[iter];
        ++iter;
        auto rhs = parse_mul(lexes, iter);
        std::unique_ptr<Binop> tmp(new Binop);
        tmp->op_ = (op == "+") ? PLUS : MINUS;
        tmp->lhs_ = std::move(lhs);
        tmp->rhs_ = std::move(rhs);
        lhs = std::move(tmp);
    }
    return lhs;
}

bool is_cmp(const Lexes& lexes, int iter)
{
    return lexes[iter] == "<"
        || lexes[iter] == ">"
        || lexes[iter] == "="
        || (lexes[iter] == "!" && lexes[iter + 1] == "=");
}

token_type parse_cmp_op(const Lexes& lexes, int& iter)
{
    if (lexes[iter] == "<")
    {
        if (lexes[iter + 1] == "=")
        {
            iter += 2;
            return LESSEQ;
        }
        else
        {
            ++iter;
            return LESS;
        }
    }

    if (lexes[iter] == ">")
    {
        if (lexes[iter + 1] == "=")
        {
            iter += 2;
            return GREATEREQ;
        }
        else
        {
            ++iter;
            return GREATER;
        }
    }

    if (lexes[iter + 1] == "=")
    {
        if (lexes[iter] == "=")
        {
            iter += 2;
            return EQUAL;
        }
        else if (lexes[iter] == "!")
        {
            iter += 2;
            return NEQUAL;
        }
    }
    throw "not a comparison operator";
}

std::unique_ptr<Exp> parse_cmp(const Lexes& lexes, int& iter)
{
    auto lhs = parse_add(lexes, iter);

    while (is_cmp(lexes, iter))
    {
        token_type op = parse_cmp_op(lexes, iter);
        auto rhs = parse_add(lexes, iter);
        std::unique_ptr<Binop> tmp(new Binop);
        tmp->op_ = op;
        tmp->lhs_ = std::move(lhs);
        tmp->rhs_ = std::move(rhs);
        lhs = std::move(tmp);
    }
    return lhs;
}

std::unique_ptr<Instr> parse_instr(const Lexes& lexes, int& iter)
{
    std::unique_ptr<Instr> res;

    if (lexes[iter] == "if")
        return nullptr;
    if (lexes[iter] == "until")
        return nullptr;

    if (lexes[iter] == "return")
    {
        ++iter;
        std::unique_ptr<Return> ret(new Return);
        ret->e_ = parse_cmp(lexes, iter);
        res = std::move(ret);
    }
    else if (lexes[iter + 1] == "=")
    {
        std::string ident = lexes[iter];
        iter += 2;
        auto val = parse_add(lexes, iter);
        res = std::unique_ptr<VarSet>(new VarSet(ident, val.release()));
    }
    else
    {
        std::unique_ptr<NakedExpr> ne(new NakedExpr);
        ne->e_ = parse_add(lexes, iter);
        res = std::move(ne);
    }
    if (lexes[iter] == ";")
        ++iter;

    return res;
}

std::unique_ptr<IfNot> parse_if_cond(const Lexes& lexes, int& iter)
{
    if (lexes[iter] != "if")
        return nullptr;
    ++iter;
    std::unique_ptr<IfNot> if_not(new IfNot);
    if_not->cond = parse_cmp(lexes, iter);
    return if_not;
}

std::pair<int, int> parse_block(const Lexes& lexes, int& iter, Code& code)
{
    if (lexes[iter] != "{")
        throw "expect '{' at beginning of blocks";
    ++iter;

    int start_block = code.NewBlock();
    int end_block = start_block;

    while (lexes[iter] != "}")
    {
        auto instr = parse_instr(lexes, iter);
        if (instr)
            code.GetBlock(end_block).Append(instr.release());
        else if (lexes[iter] == "if")
        {
            // condition
            auto if_not = parse_if_cond(lexes, iter);

            // body
            std::pair<int, int> if_branch = parse_block(lexes, iter, code);
            if (lexes[iter] == "else")
            {
                ++iter;
                std::pair<int, int> else_branch = parse_block(lexes, iter, code);
                int new_b = code.NewBlock();
                /*
                 *              if_branch
                 *             /          \
                 *   end_block             new_b
                 *             \          /
                 *              else_branch
                 */
                code.GetBlock(if_branch.second).successors.push_back(new_b);
                code.GetBlock(else_branch.second).successors.push_back(new_b);
                code.GetBlock(end_block).successors.push_back(if_branch.first);
                code.GetBlock(end_block).successors.push_back(else_branch.first);

                if_not->true_block = else_branch.first;
                if_not->false_block = if_branch.first;
                code.GetBlock(end_block).Append(if_not.release());

                // the end_block is now new_b
                end_block = new_b;
                code.GetBlock(if_branch.second).Append(new Jmp(new_b));
            }
            else
            {
                int new_b = code.NewBlock();
                /*
                 *           _____________
                 *          /             \
                 * end_block---if_branch---new_b
                 *
                 */
                code.blocks_[if_branch.second].successors.push_back(new_b);
                code.GetBlock(end_block).successors.push_back(new_b);

                if_not->true_block = new_b;
                if_not->false_block = if_branch.first;
                code.GetBlock(end_block).Append(if_not.release());
                end_block = new_b;
            }
        }
        else if (lexes[iter] == "until")
        {
            ++iter;
            std::unique_ptr<IfNot> if_not(new IfNot);
            if_not->cond = parse_cmp(lexes, iter);
            std::pair<int, int> body = parse_block(lexes, iter, code);
            int cond_block = code.NewBlock();
            int followup = code.NewBlock();
            if_not->true_block = body.first;
            if_not->false_block = followup;

            /*                         ____>____
             *                        /         \
             * end_block -> cond_block -> body   followup
             *              \--------<-------/
             */
            code.GetBlock(cond_block).Append(if_not.release());
            code.GetBlock(end_block).successors.push_back(cond_block);
            code.GetBlock(cond_block).successors.push_back(body.first);
            code.GetBlock(cond_block).successors.push_back(followup);
            code.GetBlock(body.second).successors.push_back(cond_block);
            code.GetBlock(end_block).Append(new Jmp(cond_block));
            end_block = followup;
        }
    }
    ++iter;
    return std::make_pair(start_block, end_block);
}

void parse(const Lexes& lexes, int& iter, Code& code)
{
    parse_block(lexes, iter, code);
}
