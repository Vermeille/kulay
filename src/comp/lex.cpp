#include "lex.h"

std::vector<std::string> lex(std::istream& strm)
{
    std::vector<std::string> toks;

    while (isspace(strm.peek()))
        strm.get();

    while (!strm.eof())
    {
        std::string tok;
        if (isalnum(strm.peek()))
        {
            while (isalnum(strm.peek()))
                tok.push_back(strm.get());
            toks.push_back(tok);
        }
        else if (isspace(strm.peek()))
            while (isspace(strm.peek()))
                strm.get();
        else
            toks.push_back(std::string("") + (char)strm.get());

    }

    return toks;
}
