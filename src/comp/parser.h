#pragma once

#include <vector>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <map>
#include <string>
#include <queue>
#include "lex.h"

enum token_type
{
    PLUS,
    MINUS,
    MUL,
    DIV,
    EQUAL,
    NEQUAL,
    LESS,
    LESSEQ,
    GREATER,
    GREATEREQ,
};

struct Context
{
    std::map<std::string, int> vars;
    int nb_vars;
    Context() : nb_vars(0) {}

    int GetVarId(const std::string& name)
    {
        auto res = vars.find(name);
        if (res == std::end(vars))
            return SetVar(name);
        return res->second;
    }
    int SetVar(const std::string& name)
    {
        vars[name] = nb_vars;
        return nb_vars++;
    }
};

struct Exp
{
    virtual void Codegen(Context*) = 0;
};

struct Instr
{
    virtual void Codegen(Context*) = 0;
};

struct VarGet : public Exp
{
    VarGet(const std::string& str) : ident_(str) {}
    void Codegen(Context* ctx) override
    {
        std::cout << "  var-get " << ctx->GetVarId(ident_) << std::endl;
    }

    std::string ident_;
};

struct VarSet : public Instr
{
    VarSet(const std::string& str, Exp* val) : ident_(str), val_(val) {}

    void Codegen(Context* ctx) override
    {
        val_->Codegen(ctx);
        std::cout << "  var-set " << ctx->GetVarId(ident_) << std::endl;
    }

    std::string ident_;
    std::unique_ptr<Exp> val_;
};

struct NakedExpr : public Instr
{
    void Codegen(Context* ctx) override
    {
        e_->Codegen(ctx);
        std::cout << "  pop\n";
    }
    std::unique_ptr<Exp> e_;
};

struct Return : public Instr
{
    void Codegen(Context* ctx) override { e_->Codegen(ctx); }
    std::unique_ptr<Exp> e_;
};

struct IntLit : public Exp
{
    int val_;
    IntLit(int v) : val_(v) {}
    void Codegen(Context*) override
    {
        std::cout << "  push " << val_ << std::endl;
    }
};

struct Binop : public Exp
{
    std::unique_ptr<Exp> lhs_;
    std::unique_ptr<Exp> rhs_;
    token_type op_;

    void Codegen(Context* ctx) override
    {
        lhs_->Codegen(ctx);
        rhs_->Codegen(ctx);
        switch (op_)
        {
            case PLUS:
                std::cout << "  add\n";
                break;
            case MINUS:
                std::cout << "  sub\n";
                break;
            case MUL:
                std::cout << "  mul\n";
                break;
            case DIV:
                std::cout << "  div\n";
                break;

            case EQUAL:
                std::cout << "  eq\n";
                break;
            case NEQUAL:
                std::cout << "  neq\n";
                break;
            case LESS:
                std::cout << "  lt\n";
                break;
            case LESSEQ:
                std::cout << "  lteq\n";
                break;
            case GREATER:
                std::cout << "  gt\n";
                break;
            case GREATEREQ:
                std::cout << "  gteq\n";
                break;

            default:
                exit(1);
                break;
        }
    }
};

struct Neg : public Exp
{
    std::unique_ptr<Exp> e_;
    void Codegen(Context* ctx) override
    {
        std::cout << "  push 0\n";
        e_->Codegen(ctx);
        std::cout << "  sub\n";
    }
};

struct If : public Instr
{
    std::unique_ptr<Exp> cond;
    int true_block;
    int false_block;

    void Codegen(Context* ctx) override
    {
        cond->Codegen(ctx);
        std::cout << "  if block" << true_block;
    }
};

struct IfNot : public Instr
{
    std::unique_ptr<Exp> cond;
    int true_block;
    int false_block;

    void Codegen(Context* ctx) override
    {
        cond->Codegen(ctx);
        std::cout << "  if-not block" << true_block << "\n";
    }
};

struct Jmp : public Instr
{
    int dest_block;
    Jmp(int dst) : dest_block(dst) {}
    void Codegen(Context*) override
    {
        std::cout << "  jmp block" << dest_block << "\n";
    }
};

struct Block
{
    std::vector<std::unique_ptr<Instr>> instr;
    std::vector<int> successors;
    int id;
    void Append(Instr* e) { instr.push_back(std::unique_ptr<Instr>(e)); }

    Block(int id_) : id(id_) {}

    void Codegen(Context* ctx)
    {
        for (auto& i : instr)
            i->Codegen(ctx);
    }
};

struct Code
{
    int NewBlock()
    {
        blocks_.push_back(Block(blocks_.size()));
        return blocks_.size() - 1;
    }

    Code(const Code&) = delete;
    Code() = default;

    Block& GetBlock(int id) { return blocks_[id]; }

    void Codegen()
    {
        Context ctx;
        for (auto& b : blocks_)
        {
            std::cout << "block" << b.id << " :\n";
            b.Codegen(&ctx);
        }
    }

    void PrintGraph() const
    {
        std::cout << blocks_.size() << " blocks\n";

        for (auto& b : blocks_)
        {
            for (auto s : b.successors)
                std::cout << b.id << " -> " << s << "\n";
        }
    }

    std::vector<Block> blocks_;
};

void parse(const Lexes& lexs, int& iter, Code& code);
