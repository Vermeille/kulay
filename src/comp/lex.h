#include <vector>
#include <string>
#include <iostream>
#include <cctype>

typedef std::vector<std::string> Lexes;

std::vector<std::string> lex(std::istream& strm);
