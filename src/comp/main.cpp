#include "parser.h"
#include <unistd.h>
#include <fstream>
#include "lex.h"

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cerr << "usage: ./" << argv[0] << " <sourcefile>" << std::endl;
        return 1;
    }
    std::ifstream in(argv[1]);
    Lexes l = lex(in);
    int iter = 0;
    Code code;
    parse(l, iter, code);
    code.Codegen();
    std::cout << "exit\n";
    return 0;
}

