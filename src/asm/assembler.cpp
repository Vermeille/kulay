#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fstream>
#include <map>

std::vector<std::string> Lex(std::istream& strm)
{
    std::vector<std::string> toks;

    while (isspace(strm.peek()))
        strm.get();

    while (!strm.eof())
    {
        std::string tok;
        if (isalnum(strm.peek()) || strm.peek() == '-')
        {
            while (isalnum(strm.peek()) || strm.peek() == '-')
                tok.push_back(strm.get());
            toks.push_back(tok);
        }
        else if (isspace(strm.peek()))
            while (isspace(strm.peek()))
                strm.get();
        else
            toks.push_back(std::string("") + (char)strm.get());

    }

    return toks;
}

class Writer
{
    unsigned char* data_;
    unsigned char* writer_;
    std::map<std::string, unsigned int> labels_;
    std::map<std::string, std::vector<unsigned char*>> unresolved_;

  public:
    Writer()
    {
        data_ = static_cast<unsigned char*>(mmap(NULL, 1024*1024,
                    PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
        writer_ = data_;
    }

    void Byte(unsigned char b)
    {
        *writer_ = b;
        ++writer_;
    }

    void Int(int i)
    {
        *(int*)writer_ = i;
        writer_ += sizeof (int);
    }

    void SetLabel(const std::string& str, unsigned int addr)
    {
        labels_[str] = addr;
        for (auto& usage : unresolved_[str])
            *(unsigned int*)usage = addr;
        unresolved_.erase(str);
    }

    void Label(const std::string& str)
    {
        auto label = labels_.find(str);
        if (label == labels_.end())
        {
            unresolved_[str].push_back(writer_);
        }
        else
        {
            *(int*)writer_ = label->second;
        }
        writer_ += 4;
    }

    void Dump(const char* path)
    {
        std::ofstream ofs(path);
        ofs.write(reinterpret_cast<char*>(data_), writer_ - data_);
    }
};

int main(int argc, char** argv)
{
    std::string line_buf;
    Writer w;

    unsigned int addr = 0;
    while (std::getline(std::cin, line_buf))
    {
        std::istringstream line(line_buf);
        std::vector<std::string> tokens = Lex(line);

        std::cout << "line:" << line_buf << std::endl;
        std::cout << "token:" << tokens[0] << std::endl;
        if (tokens.size() > 1 && tokens[1] == ":")
        {
            w.SetLabel(tokens[0], addr);
            --addr;
        }
        if (tokens[0] == "push")
        {
            w.Byte(0);
            w.Int(std::atoi(tokens[1].c_str()));
            addr += 4;
        }
        else if (tokens[0] == "add") { w.Byte(1); }
        else if (tokens[0] == "sub") { w.Byte(2); }
        else if (tokens[0] == "mul") { w.Byte(3); }
        else if (tokens[0] == "exit") { w.Byte(4); }
        else if (tokens[0] == "var-get")
        {
            w.Byte(5);
            w.Byte(std::atoi(tokens[1].c_str()));
            ++addr;
        }
        else if (tokens[0] == "var-set")
        {
            w.Byte(6);
            w.Byte(std::atoi(tokens[1].c_str()));
            ++addr;
        }
        else if (tokens[0] == "pop") { w.Byte(7); }
        else if (tokens[0] == "eq") { w.Byte(8); }
        else if (tokens[0] == "neq") { w.Byte(9); }
        else if (tokens[0] == "lt") { w.Byte(10); }
        else if (tokens[0] == "lteq") { w.Byte(11); }
        else if (tokens[0] == "gt") { w.Byte(12); }
        else if (tokens[0] == "gteq") { w.Byte(13); }
        else if (tokens[0] == "if-not")
        {
            w.Byte(14);
            w.Label(tokens[1]);
            addr += 4;
        }
        else if (tokens[0] == "jmp")
        {
            w.Byte(15);
            w.Label(tokens[1]);
            addr += 4;
        }
        ++addr;
    }

    w.Dump(argv[1]);

    return 0;
}
