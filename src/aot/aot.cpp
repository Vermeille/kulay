#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdint.h>
#include <sys/mman.h>

#include <x86/emitter.h>
#include <common/opcodes.h>
#include <common/vm.h>

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "usage: ./%s <executable>\n", argv[0]);
        return 1;
    }
    struct stat sb;
    char *prgm;

    int fd = open(argv[1], O_RDONLY);
    fstat(fd, &sb);

    prgm = (char*)mmap(NULL, sb.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0);

    VM vm(prgm);

    vm.Compile();

    return 0;
}
